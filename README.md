# nrgillumos

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nrgillumos](#setup)
    * [What nrgillumos affects](#what-nrgillumos-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nrgillumos](#beginning-with-nrgillumos)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module manages Illumos (OpenSolaris fork) configuration on NRG nodes.  This module is 
intended to deploy whatever shared, Illumos/OpenSolaris-specific configuration that is 
independent of NRG's custom projects and applications.  For example, custom package repos, 
Solaris-specific sshd configuration.

This module will be one of the first (or the very first) classes run on a new node, so
it needs to reasonably self-standing, no dependencies on services not already available on
a vanilla VM.  This module can (and does) depend on others included in the Puppetfile
stored in the Puppet control repo.

General NRG configuration that tends to be more OS-agnostic (i.e. NTP configuration) is 
better handled by the base profile class in the Puppet control repo at 
https://bitbucket.org/NRGpuppet/puppet_control/src or in a dedicated custom module.

## Module Description

If applicable, this section should have a brief description of the technology
the module integrates with and what that integration enables. This section
should answer the questions: "What does this module *do*?" and "Why would I use
it?"

If your module has a range of functionality (installation, configuration,
management, etc.) this is the time to mention it.

## Setup

### What nrgillumos affects

* A list of files, packages, services, or operations that the module will alter,
  impact, or execute on the system it's installed on.
* This is a great place to stick any warnings.
* Can be in list or paragraph form.

### Setup Requirements **OPTIONAL**

If your module requires anything extra before setting up (pluginsync enabled,
etc.), mention it here.

### Beginning with nrgillumos

The very basic steps needed for a user to get the module up and running.

If your most recent release breaks compatibility or requires particular steps
for upgrading, you may wish to include an additional section here: Upgrading
(For an example, see http://forge.puppetlabs.com/puppetlabs/firewall).

## Usage

Put the classes, types, and resources for customizing, configuring, and doing
the fancy stuff with your module here.

## Reference

Here, list the classes, types, providers, facts, etc contained in your module.
This section should include all of the under-the-hood workings of your module so
people know what the module is touching on their system but don't need to mess
with things. (We are working on automating this section!)

## Limitations

This is where you list OS compatibility, version compatibility, etc.

## Development

Since your module is awesome, other users will want to play with it. Let them
know what the ground rules for contributing are.

## Release Notes/Contributors/Etc **Optional**

If you aren't using changelog, put your release notes here (though you should
consider using changelog). You may also add any additional sections you feel are
necessary or important to include here. Please use the `## ` header.
