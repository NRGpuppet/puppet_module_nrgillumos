# Class: nrgillumos::private::packages
# ===============================
#
# Private class to handle package installation for OmniOS.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::private::packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgillumos::private::packages {
  
  include '::nrgillumos'

  case $kernelversion {

    /omnios-r151024(.*)/: {
      $package_list = [ 'developer/library/lint',
                        'developer/parser/bison',
                        'developer/lexer/flex',
                        'developer/versioning/git',
                        'developer/versioning/mercurial',
                        'developer/object-file',
                        'system/header',
                        'gcc5',
                        'gnu-make' ]
    }
    /omnios-r151030(.*)/: {
      $package_list = [ 'developer/parser/bison',
                        'developer/lexer/flex',
                        'developer/versioning/git',
                        'developer/object-file',
                        'system/header',
                        'gcc5',
                        'gnu-make' ]
    }
    /omnios-r151034(.*)/: {
      $package_list = [ 'developer/parser/bison',
                        'developer/lexer/flex',
                        'developer/versioning/git',
                        'developer/object-file',
                        'system/header',
                        'developer/gcc7',
                        'gnu-make' ]
    }
    /omnios-r151038(.*)/: {
      $package_list = [ 'developer/parser/bison',
                        'developer/lexer/flex',
                        'developer/versioning/git',
                        'developer/object-file',
                        'system/header',
                        'developer/gcc7',
                        'gnu-make' ]
    }


    default: {
      $package_list = [ 'developer/library/lint',
                        'developer/parser/bison',
                        'developer/lexer/flex',
                        'developer/versioning/git',
                        'developer/versioning/mercurial',
                        'system/header',
                        'gcc51',
                        'gnu-make' ]
    }

  } #case 

  package {
    $package_list:
      ensure   => present,
      provider => $::nrgillumos::pkg_provider,
      install_options  => $::nrgillumos::pkg_install_options,
  }
}
