# Class: nrgillumos::private::opencsw_packages
# ===============================
#
# Private class to handle pkgsrc package installation for OmniOS.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# Variables used by this module.
#
# * `nrgillumos::pkgsrc_pkg_provider`
#  The package provider to use when installing OpenCSW packages.  Default: pkgutil.
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::private::pkgsrc_packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2017 Chip Schweiss, unless otherwise noted.
#

class nrgillumos::private::pkgsrc_packages {
  
  include '::nrgillumos'

  $package_list = [ 'gsasl',
                    'cmake',
                    'mutt',
                    'top',
                    'bc',
                    'tcpdump',
                    'gcc7',
                    'py27-py',
                    'nagios-plugins' ]
    
  package {
    $package_list:
      ensure          => latest,
      install_options => ['-y'],
      provider        => $::nrgillumos::pkgsrc_pkg_provider;
  }
  
}
