# This is the private class nrgillumos::private::pkgsrc, and it should not be
# included explicitly.
#
# This class manages pkgsrc, and it is included by nrgillumos::init.
#
class nrgillumos::private::pkgsrc {

  include '::nrgillumos'

  file {
    'opt_pkgsrc':
      path   => '/opt/pkgsrc',
      ensure =>  directory;
  }

  $bootstrap_tar = "bootstrap-${::nrgillumos::pkgsrc_version}-x86_64.tar.gz"

  exec {
    'get_pkgsrc':
      command => "curl -O https://pkgsrc.joyent.com/packages/SmartOS/bootstrap/${bootstrap_tar}",
      cwd     => '/opt/pkgsrc',
      creates => "/opt/pkgsrc/${bootstrap_tar}",
      requires =>  File['opt_pkgsrc'],
      notify  => Exec['check_sha_pkgsrc'];
  
    'check_sha_pkgsrc':
      command     => "[ \"${::nrgillumos::pkgsrc_sha}\" == \"$(/bin/digest -a sha1 $bootstrap_tar)\" ]"
      refreshonly => true,
      notify      => Exec['extract_pkgsrc'];

    'extract_pkgsrc':
      command     => "tar -zxpf /opt/pkgsrc/${bootstrap_tar} -C /",
      refreshonly => true;
  
  }

}
      
      
