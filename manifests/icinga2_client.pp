# Class: nrgillumos::icinga2_client
# ===============================
#
# Provisional support for managing Icinga2 client under OmniOS/Illumos, coded to provide
# some meager compatibility with the parameters also used by Icinga/icinga2 module. 
# Ideally, a Forge module will eventually render this class obsolete.
#
# This class needs to be declared explicitly, so that it can receive parameters
# and also satisfy class ordering requirements.  This class includes nrgillumos::init.
#
# Refs: 
# http://blog.ls-al.com/icinga2-on-solaris-11/
# https://dev.icinga.org/issues/8159
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# * `master_endpoint_host`
#  Required, the FQDN of the master endpoint host
#
# * `master_endpoint_post`
#  The port of the master endpoint host, which has to be passed as a string instead of
#  integer.  Default '5665'.
#
# * `master_zone`
#  The name of the master zone my zone should be child to, default 'master'
#
# Variables
# ----------
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::icinga2_client':
#      master_endpoint_host => 'icinga.mydomain.com',
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgillumos::icinga2_client (
  String $icinga2_fetch_url,
  String $icinga2_md5,
  String $master_endpoint_host,
  String $master_zone = 'master',
  String $master_endpoint_port = '5665',
  String $checkplugin_libdir = '/opt/csw/libexec/nagios-plugins/',
  String $config_owner = 'icinga',
  String $config_group = 'icinga',
  String $config_mode  = '0640',
  String $command_group = 'icingacm',
  String $service_name = 'network/icinga2',
  String $install_prefix = '/usr/local',
  Boolean $manage_service = true,
  ) {

  require ::nrgillumos  
  require ::nrgillumos::private::opencsw_packages

  $service_notify = $manage_service ? {
    true => Service[$service_name],
    default => undef,
  }

  file { $install_prefix:
    ensure => directory,
  }

  include ::nrgillumos::private::icinga2_client_install
  include ::nrgillumos::private::icinga2_client_config
  File["${install_prefix}"] -> Class['Nrgillumos::Private::Icinga2_client_install']
  File["${install_prefix}"] -> Class['Nrgillumos::Private::Icinga2_client_config']

  # Import SMF manifest
  exec { 'icinga2_smf_manifest_import_solaris':
    command => 'svcadm restart manifest-import',
    unless => 'svcs -a | grep icinga2',
    require => [Class['Nrgillumos::Private::Icinga2_client_install'], Class['Nrgillumos::Private::Icinga2_client_config']],
    notify => $service_notify;
  }

  # Define icinga2 service and enable it.
  if $manage_service {
    service { $service_name:
      ensure  => running,
      enable  => true,
      subscribe => Class['Nrgillumos::Private::Icinga2_client_config'],
    }
  }

}
