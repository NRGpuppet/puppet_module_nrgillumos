#! /bin/bash

# update hostid

# Proceedure from Ben Rockwood
# http://www.cuddletech.com/blog/pivot/entry.php?id=1128

# First trim existing value from /etc/hostid
echo '# DO NOT EDIT' > /etc/hostid

# Now append new hostid
echo "0x${1}" | \
  /usr/perl5/bin/perl -e \
  'print("\""); while(<STDIN>){chop;tr/!-~/P-~!-O/;print $_;} print("\"\n"); exit 0;' \
  >> /etc/hostid

/sbin/bootadm update-archive

touch /tmp/hostid_pending
